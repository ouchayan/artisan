<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220821171746 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE admin (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_880E0D76F85E0677 (username), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE artisan (id INT AUTO_INCREMENT NOT NULL, base_city_id INT NOT NULL, job_id INT NOT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, pseudo VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, phone_number VARCHAR(255) NOT NULL, summary LONGTEXT NOT NULL, description LONGTEXT NOT NULL, number_experience INT NOT NULL, avatar VARCHAR(255) DEFAULT NULL, verified TINYINT(1) DEFAULT NULL, enabled TINYINT(1) DEFAULT NULL, INDEX IDX_3C600AD3BFAADBCB (base_city_id), INDEX IDX_3C600AD3BE04EA9 (job_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE artisan_city (artisan_id INT NOT NULL, city_id INT NOT NULL, INDEX IDX_EAB2251D5ED3C7B7 (artisan_id), INDEX IDX_EAB2251D8BAC62AF (city_id), PRIMARY KEY(artisan_id, city_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE artisan_tag (artisan_id INT NOT NULL, tag_id INT NOT NULL, INDEX IDX_3D062AF85ED3C7B7 (artisan_id), INDEX IDX_3D062AF8BAD26311 (tag_id), PRIMARY KEY(artisan_id, tag_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE city (id INT AUTO_INCREMENT NOT NULL, region_id INT NOT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_2D5B023498260155 (region_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE gallery (id INT AUTO_INCREMENT NOT NULL, artisan_id INT NOT NULL, url VARCHAR(255) NOT NULL, sort INT NOT NULL, INDEX IDX_472B783A5ED3C7B7 (artisan_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE job (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, picture_url VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE level (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE picture (id INT AUTO_INCREMENT NOT NULL, project_id INT DEFAULT NULL, artisan_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, INDEX IDX_16DB4F89166D1F9C (project_id), INDEX IDX_16DB4F895ED3C7B7 (artisan_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE project (id INT AUTO_INCREMENT NOT NULL, city_id INT NOT NULL, artisan_id INT NOT NULL, title VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, date DATE NOT NULL, pictures LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', INDEX IDX_2FB3D0EE8BAC62AF (city_id), INDEX IDX_2FB3D0EE5ED3C7B7 (artisan_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE project_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE region (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE skill (id INT AUTO_INCREMENT NOT NULL, level_id INT NOT NULL, artisan_id INT NOT NULL, sort INT DEFAULT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_5E3DE4775FB14BA7 (level_id), INDEX IDX_5E3DE4775ED3C7B7 (artisan_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tag (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE messenger_messages (id BIGINT AUTO_INCREMENT NOT NULL, body LONGTEXT NOT NULL, headers LONGTEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at DATETIME NOT NULL, available_at DATETIME NOT NULL, delivered_at DATETIME DEFAULT NULL, INDEX IDX_75EA56E0FB7336F0 (queue_name), INDEX IDX_75EA56E0E3BD61CE (available_at), INDEX IDX_75EA56E016BA31DB (delivered_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE artisan ADD CONSTRAINT FK_3C600AD3BFAADBCB FOREIGN KEY (base_city_id) REFERENCES city (id)');
        $this->addSql('ALTER TABLE artisan ADD CONSTRAINT FK_3C600AD3BE04EA9 FOREIGN KEY (job_id) REFERENCES job (id)');
        $this->addSql('ALTER TABLE artisan_city ADD CONSTRAINT FK_EAB2251D5ED3C7B7 FOREIGN KEY (artisan_id) REFERENCES artisan (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE artisan_city ADD CONSTRAINT FK_EAB2251D8BAC62AF FOREIGN KEY (city_id) REFERENCES city (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE artisan_tag ADD CONSTRAINT FK_3D062AF85ED3C7B7 FOREIGN KEY (artisan_id) REFERENCES artisan (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE artisan_tag ADD CONSTRAINT FK_3D062AF8BAD26311 FOREIGN KEY (tag_id) REFERENCES tag (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE city ADD CONSTRAINT FK_2D5B023498260155 FOREIGN KEY (region_id) REFERENCES region (id)');
        $this->addSql('ALTER TABLE gallery ADD CONSTRAINT FK_472B783A5ED3C7B7 FOREIGN KEY (artisan_id) REFERENCES artisan (id)');
        $this->addSql('ALTER TABLE picture ADD CONSTRAINT FK_16DB4F89166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
        $this->addSql('ALTER TABLE picture ADD CONSTRAINT FK_16DB4F895ED3C7B7 FOREIGN KEY (artisan_id) REFERENCES artisan (id)');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EE8BAC62AF FOREIGN KEY (city_id) REFERENCES city (id)');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EE5ED3C7B7 FOREIGN KEY (artisan_id) REFERENCES artisan (id)');
        $this->addSql('ALTER TABLE skill ADD CONSTRAINT FK_5E3DE4775FB14BA7 FOREIGN KEY (level_id) REFERENCES level (id)');
        $this->addSql('ALTER TABLE skill ADD CONSTRAINT FK_5E3DE4775ED3C7B7 FOREIGN KEY (artisan_id) REFERENCES artisan (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE artisan_city DROP FOREIGN KEY FK_EAB2251D5ED3C7B7');
        $this->addSql('ALTER TABLE artisan_tag DROP FOREIGN KEY FK_3D062AF85ED3C7B7');
        $this->addSql('ALTER TABLE gallery DROP FOREIGN KEY FK_472B783A5ED3C7B7');
        $this->addSql('ALTER TABLE picture DROP FOREIGN KEY FK_16DB4F895ED3C7B7');
        $this->addSql('ALTER TABLE project DROP FOREIGN KEY FK_2FB3D0EE5ED3C7B7');
        $this->addSql('ALTER TABLE skill DROP FOREIGN KEY FK_5E3DE4775ED3C7B7');
        $this->addSql('ALTER TABLE artisan DROP FOREIGN KEY FK_3C600AD3BFAADBCB');
        $this->addSql('ALTER TABLE artisan_city DROP FOREIGN KEY FK_EAB2251D8BAC62AF');
        $this->addSql('ALTER TABLE project DROP FOREIGN KEY FK_2FB3D0EE8BAC62AF');
        $this->addSql('ALTER TABLE artisan DROP FOREIGN KEY FK_3C600AD3BE04EA9');
        $this->addSql('ALTER TABLE skill DROP FOREIGN KEY FK_5E3DE4775FB14BA7');
        $this->addSql('ALTER TABLE picture DROP FOREIGN KEY FK_16DB4F89166D1F9C');
        $this->addSql('ALTER TABLE city DROP FOREIGN KEY FK_2D5B023498260155');
        $this->addSql('ALTER TABLE artisan_tag DROP FOREIGN KEY FK_3D062AF8BAD26311');
        $this->addSql('DROP TABLE admin');
        $this->addSql('DROP TABLE artisan');
        $this->addSql('DROP TABLE artisan_city');
        $this->addSql('DROP TABLE artisan_tag');
        $this->addSql('DROP TABLE city');
        $this->addSql('DROP TABLE gallery');
        $this->addSql('DROP TABLE job');
        $this->addSql('DROP TABLE level');
        $this->addSql('DROP TABLE picture');
        $this->addSql('DROP TABLE project');
        $this->addSql('DROP TABLE project_type');
        $this->addSql('DROP TABLE region');
        $this->addSql('DROP TABLE skill');
        $this->addSql('DROP TABLE tag');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
