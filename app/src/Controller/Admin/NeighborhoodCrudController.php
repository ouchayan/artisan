<?php

namespace App\Controller\Admin;

use App\Entity\City;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use App\Entity\Neighborhood;

class NeighborhoodCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Neighborhood::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        yield IdField::new('id')->onlyOnIndex();
        yield Field::new('name');
        yield AssociationField::new('city');
    }
}
