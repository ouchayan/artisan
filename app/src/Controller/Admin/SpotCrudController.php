<?php

namespace App\Controller\Admin;

use App\Entity\Spot;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;

class SpotCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Spot::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        yield FormField::addPanel('Details')->collapsible();;
        yield IdField::new('id')->onlyOnIndex();
        yield CollectionField::new('galleries')->useEntryCrudForm();
        yield CollectionField::new('paragraphs')->useEntryCrudForm();
        yield CollectionField::new('facilities')->useEntryCrudForm();
        yield FormField::addPanel('Details')->collapsible();
        yield Field::new('title')->setColumns(6);
        yield Field::new('lotissement')->setColumns(6);
        yield AssociationField::new('user')->setColumns(4);
        yield AssociationField::new('spotType')->setColumns(4);
        yield AssociationField::new('neighborhood')->setColumns(4);
        yield AssociationField::new('facade', 'عدد الواجهات')->setColumns(4);
        yield AssociationField::new('stage','عدد الطوابق')->setColumns(4);
        yield Field::new('surface','المساحة')->setColumns(4);
        yield Field::new('price','سعر البقعة')->setColumns(4);
        yield BooleanField::new('published')->setColumns(4);
        yield BooleanField::new('verified')->setColumns(4);
        yield FormField::addPanel('Details')->collapsible();
        yield Field::new('location','location')->setColumns(12);
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            // ...
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->add(Crud::PAGE_EDIT, Action::SAVE_AND_ADD_ANOTHER)
        ;
    }
}
