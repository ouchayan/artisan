<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Entity\Spot;
use App\Entity\Market;
use App\Entity\City;
use App\Entity\SpotType;
use App\Entity\Region;
use App\Entity\Facade;
use App\Entity\Stage;
use App\Entity\Tag;
use App\Entity\Neighborhood;
use App\Entity\UserType;
use App\Entity\Item;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        return $this->render('admin/index.html.twig');

        // Option 1. You can make your dashboard redirect to some common page of your backend
        //
        // $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
        // return $this->redirect($adminUrlGenerator->setController(OneOfYourCrudController::class)->generateUrl());

        // Option 2. You can make your dashboard redirect to different pages depending on the user
        //
        // if ('jane' === $this->getUser()->getUsername()) {
        //     return $this->redirect('...');
        // }

        // Option 3. You can render some custom template to display a proper dashboard with widgets, etc.
        // (tip: it's easier if your template extends from @EasyAdmin/page/content.html.twig)
        //
        // return $this->render('some/path/my-dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('بقع و متاجر للبيع')
            ->setTextDirection('ltr');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');

        yield MenuItem::linkToCrud('Terrains à vendre', 'fa fa-user', Spot::class);
        yield MenuItem::linkToCrud('Magasins à vendre', 'fa fa-user', Market::class);
        yield MenuItem::linkToCrud('Courtiers/Propriétaires', 'fa fa-user', User::class);

        yield MenuItem::section('Paramétrage', 'fa fa-gear');
        yield MenuItem::linkToCrud('Régions', 'fa fa-image', Region::class);
        yield MenuItem::linkToCrud('Villes', 'fa fa-city', City::class);
        yield MenuItem::linkToCrud('Quartiers', 'fa fa-city', Neighborhood::class);
        yield MenuItem::linkToCrud('Type de terrain', 'fa fa-shield-blank', SpotType::class);
        yield MenuItem::linkToCrud('Façades', 'fa fa-shield-blank', Facade::class);
        yield MenuItem::linkToCrud('Etages', 'fa fa-shield-blank', Stage::class);
        yield MenuItem::linkToCrud("Type d'utilisateur", 'fa fa-shield-blank', UserType::class);
        yield MenuItem::linkToCrud("Items", 'fa fa-shield-blank', Item::class);
        yield MenuItem::linkToCrud('Tags', 'fa fa-tags', Tag::class);
    }
}
