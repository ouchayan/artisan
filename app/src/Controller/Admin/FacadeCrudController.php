<?php

namespace App\Controller\Admin;

use App\Entity\Facade;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class FacadeCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Facade::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
