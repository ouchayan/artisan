<?php

namespace App\Controller\Admin;

use App\Entity\Facility;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;

class FacilityCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Facility::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
            Field::new('icon'),
            Field::new('title'),
            Field::new('description')
        ];
    }
}
