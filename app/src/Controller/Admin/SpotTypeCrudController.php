<?php

namespace App\Controller\Admin;

use App\Entity\SpotType;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class SpotTypeCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return SpotType::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
