<?php

namespace App\Controller;

use App\Entity\Spot;
use App\Repository\SpotRepository;
use App\Repository\SpotTypeRepository;
use App\Repository\NeighborhoodRepository;
use App\Repository\FacadeRepository;
use App\Repository\StageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Filter\SpotFilter;

class SpotController extends AbstractController
{
    
    #[Route('/spots', name: 'app_spot')]
    public function index(
        Request $request,
        SpotRepository $spotRepository,
        SpotTypeRepository $spotTypeRepository,
        FacadeRepository $facadeRepository,
        StageRepository $stageRepository,
        NeighborhoodRepository $neighborhoodRepository
    ): Response
    {
        $spotTypes = $spotTypeRepository->findAll();
        $facades = $facadeRepository->findAll();
        $stages = $stageRepository->findAll();
        $neighborhoods = $neighborhoodRepository->findAll();

        $spotFilter = new SpotFilter($request);

        $spots = $spotRepository->findWithCriteria($spotFilter);

        return $this->render('spot/index.html.twig', [
            'spots' => $spots,
            'spotTypes' => $spotTypes,
            'facades' => $facades,
            'stages' => $stages,
            'neighborhoods' => $neighborhoods,
            'spotFilter' => $spotFilter
        ]);
    }

    #[Route('/spot/{id}', name: 'app_spot_show')]
    public function show(Spot $spot): Response
    {
        return $this->render('spot/show.html.twig', [
            'spot' => $spot
        ]);
    }

    #[Route('/spot/similar', name: 'app_spot_similar')]
    public function similar(SpotRepository $spotRepository,): Response
    {
        $spots = $spotRepository->getSimilaire(3);

        return $this->render('spot/similar.html.twig', [
            'spots' => $spots
        ]);
    }
}
