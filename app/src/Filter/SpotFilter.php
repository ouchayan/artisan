<?php

namespace App\Filter;

use Symfony\Component\HttpFoundation\Request;

class SpotFilter
{

    public ?int $spotType = null;
    public ?int $facade = null;
    public ?int $stage = null;
    public ?int $neighborhood = null;


    public function __construct(Request $request)
    {
        $this->spotType = $request->query->get('spotType');
        $this->facade = $request->query->get('facade');
        $this->stage = $request->query->get('stage');
        $this->neighborhood = $request->query->get('neighborhood');
    }
}
