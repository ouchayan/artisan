<?php

namespace App\DataFixtures;

use App\Entity\ProjectType;
use Doctrine\Persistence\ObjectManager;

class ProjectTypeFixtures extends BaseFixture
{
    protected $faker;

    public function loadData(ObjectManager $manager)
    {
        $this->createMany(ProjectType::class, 100, function(ProjectType $projectType, $count) {
            $projectType->setName($this->faker->asciify('projectType-****'));
        });

        $manager->flush();
    }
}
