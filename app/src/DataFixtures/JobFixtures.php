<?php

namespace App\DataFixtures;

use App\Entity\City;
use App\Entity\Job;
use Doctrine\Persistence\ObjectManager;

class JobFixtures extends BaseFixture
{
    protected $faker;

    public function loadData(ObjectManager $manager)
    {
        $this->createMany(Job::class, 100, function(Job $job, $count) {
            $job->setName($this->faker->asciify('job-****'));
        });

        $manager->flush();
    }
}
