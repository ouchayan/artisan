<?php

namespace App\DataFixtures;

use App\Entity\Region;
use Doctrine\Persistence\ObjectManager;

class RegionFixtures extends BaseFixture
{
    protected $faker;

    public function loadData(ObjectManager $manager)
    {
        $this->createMany(Region::class, 100, function(Region $region, $count) {
            $region->setName($this->faker->asciify('region-****'));
        });

        $manager->flush();
    }
}
