<?php

namespace App\DataFixtures;

use App\Entity\Level;
use Doctrine\Persistence\ObjectManager;

class LevelFixtures extends BaseFixture
{
    protected $faker;

    public function loadData(ObjectManager $manager)
    {
        $this->createMany(Level::class, 100, function(Level $level, $count) {
            $level->setName($this->faker->asciify('level-****'));
        });

        $manager->flush();
    }
}
