<?php

namespace App\DataFixtures;

use App\Entity\Tag;
use Doctrine\Persistence\ObjectManager;

class TagFixtures extends BaseFixture
{
    protected $faker;

    public function loadData(ObjectManager $manager)
    {
        $this->createMany(Tag::class, 100, function(Tag $tag, $count) {
            $tag->setName($this->faker->asciify('tag-****'));
        });

        $manager->flush();
    }
}
