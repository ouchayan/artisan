<?php

namespace App\DataFixtures;

use App\Entity\City;
use App\Entity\Region;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class CityFixtures extends BaseFixture implements DependentFixtureInterface
{
    protected $faker;

    public function loadData(ObjectManager $manager)
    {
        $this->createMany(City::class, 100, function(City $city, $count) {
            $city->setName($this->faker->asciify('city-****'));
            $city->setRegion($this->getReference(Region::class. '_' . $this->faker->numberBetween(0, 99)));
        });

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            RegionFixtures::class
        ];
    }
}
