<?php

namespace App\Repository;

use App\Entity\Spot;
use App\Filter\SpotFilter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Spot>
 *
 * @method Spot|null find($id, $lockMode = null, $lockVersion = null)
 * @method Spot|null findOneBy(array $criteria, array $orderBy = null)
 * @method Spot[]    findAll()
 * @method Spot[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SpotRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Spot::class);
    }

    public function add(Spot $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Spot $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

   /**
    * @return Spot[] Returns an array of Spot objects
    */
   public function findWithCriteria(SpotFilter $spotFilter): array
   {
        $qb = $this->createQueryBuilder('s');

        if ($spotFilter->spotType) {
            $qb->where('s.spotType = :spotType');
            $qb->setParameter('spotType', $spotFilter->spotType);
        }

        if ($spotFilter->facade) {
            $qb->andWhere('s.facade = :facade');
            $qb->setParameter('facade', $spotFilter->facade);
        }

        if ($spotFilter->stage) {
            $qb->andWhere('s.stage = :stage');
            $qb->setParameter('stage', $spotFilter->stage);
        }

        if ($spotFilter->neighborhood) {
            $qb->andWhere('s.neighborhood = :neighborhood');
            $qb->setParameter('neighborhood', $spotFilter->neighborhood);
        }

        dump($spotFilter);

        $qb->orderBy('s.id', 'ASC');
        return $qb->getQuery()->getResult();
   }

   public function getSimilaire(int $max): array
   {
       return $this->createQueryBuilder('s')
           ->setMaxResults($max)
           ->getQuery()
           ->getResult()
       ;
   }
}
